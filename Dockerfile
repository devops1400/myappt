FROM mcr.microsoft.com/dotnet/aspnet:6.0-focal AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0-focal AS build
WORKDIR /src
COPY ["src/MyAppT/MyAppT.csproj", "src/MyAppT/"]
RUN dotnet restore "src/MyAppT/MyAppT.csproj"
COPY . .
WORKDIR "/src/src/MyAppT"
RUN dotnet build "MyAppT.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MyAppT.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENV ASPNETCORE_URLS=http://*:5000
ENTRYPOINT ["dotnet", "MyAppT.dll"]

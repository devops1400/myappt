﻿using Allure.Xunit.Attributes;
using Allure.Commons;
using Allure.Xunit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UITest
{
    [AllureOwner("AG")]
    [AllureTag("TAG-ALL")]
    [AllureEpic("TestEpic")]
    [AllureParentSuite("AllTests")]
    [AllureSuite("UITest")]
    public class UITest : IDisposable
    {
        private readonly IWebDriver driver;
        public UITest()
        {
            driver = new ChromeDriver();
        }
        public void Dispose()
        {
            driver.Quit();
            driver.Dispose();
        }

        [AllureXunit(DisplayName = "Create_GET_ReturnsCreateView")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureTag("CI")]
        [AllureSubSuite("UITest1-Automation")]

        public void Create_GET_ReturnsCreateView()
        {
            driver.Navigate().GoToUrl("https://localhost:44386/Register/Create");

            Assert.Equal("Create Record - MyAppT", driver.Title);
            Assert.Contains("Create Record", driver.PageSource);
        }

        [AllureXunit(DisplayName = "Create_POST_InvalidModel")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureTag("CI")]
        [AllureSubSuite("UITest1-Automation")]
        public void Create_POST_InvalidModel()
        {
            driver.Navigate().GoToUrl("https://localhost:44386/Register/Create");

            driver.FindElement(By.Id("Name")).SendKeys("Test");

            driver.FindElement(By.Id("Age")).SendKeys("30");

            driver.FindElement(By.ClassName("btn-primary")).Click();

            var errorMessage = driver.FindElement(By.CssSelector(".validation-summary-errors > ul > li")).Text;

            Assert.Equal("The field Age must be between 40 and 60.", errorMessage);
        }

        [AllureXunit(DisplayName = "Create_POST_ValidModel")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureTag("CI")]
        [AllureSubSuite("UITest1-Automation")]
        public void Create_POST_ValidModel()
        {
            driver.Navigate().GoToUrl("https://localhost:44386/Register/Create");

            driver.FindElement(By.Id("Name")).SendKeys("Test");

            driver.FindElement(By.Id("Age")).SendKeys("40");

            driver.FindElement(By.ClassName("btn-primary")).Click();

            Assert.Equal("Records - MyAppT", driver.Title);
            Assert.Contains("Test", driver.PageSource);
            Assert.Contains("40", driver.PageSource);
        }
    }
}
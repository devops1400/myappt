﻿using Microsoft.EntityFrameworkCore;
using MyAppT.Models;
using Allure.Commons;
using Allure.Xunit;
using Allure.Xunit.Attributes;

namespace UnitTest
{
    [AllureOwner("AG")]
    [AllureTag("TAG-ALL")]
    [AllureEpic("TestEpic")]
    [AllureParentSuite("AllTests")]
    [AllureSuite("UnitTest")]
    public class InMemoryTest : TestRegistration
    {
        
        public InMemoryTest()
            : base(
                new DbContextOptionsBuilder<AppDbContext>()
                    .UseInMemoryDatabase("TestDatabase")
                    .Options)
        {
        }
    }
}
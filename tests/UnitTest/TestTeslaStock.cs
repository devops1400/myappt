﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Allure.Commons;
using Allure.Xunit;
using Allure.Xunit.Attributes;
using MyAppT.Infrastructure;

namespace UnitTest
{
    [AllureOwner("AG")]
    [AllureTag("TAG-ALL")]
    [AllureEpic("TestEpic")]
    [AllureParentSuite("AllTests")]
    [AllureSuite("UnitTest")]
    public class TestTeslaStock
    {
        [AllureXunit(DisplayName = "Test_Entry_GET_ReturnsViewResultNullModel")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureTag("CI")]
        [AllureSubSuite("UnitTest1-Automation")]
        public void Test_Predict()
        {
            // Arrange
            TeslaStock TS = new TeslaStock();

            // Act
            int newValue = TS.Predict(800);

            // Assert
            Assert.Equal(1200, newValue);
        }


        [AllureXunitTheory]
        [AllureSubSuite("Test_PredictMultiple")]
        [InlineData(200)]
        [InlineData(400)]
        [InlineData(800)]
        public void Test_PredictMultiple(int currentValue)
        {
            // Arrange
            TeslaStock TS = new TeslaStock();

            // Act
            int newValue = TS.Predict(currentValue);
            int calculate = Convert.ToInt32(currentValue + (.5 * currentValue));

            // Assert
            Assert.Equal(currentValue + (.5 * currentValue), newValue);
        }
    }
}
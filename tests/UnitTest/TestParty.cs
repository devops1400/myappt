﻿using Xunit;
using Allure.Commons;
using Allure.Xunit;
using Allure.Xunit.Attributes;
using Microsoft.AspNetCore.Mvc;
using MyAppT.Controllers;

namespace UnitTest
{
    [AllureOwner("AG")]
    [AllureTag("TAG-ALL")]
    [AllureEpic("TestEpic")]
    [AllureParentSuite("AllTests")]
    [AllureSuite("UnitTest")]
    public class TestParty
    {
        [AllureXunit(DisplayName = "Test_Entry_GET_ReturnsViewResultNullModel")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureTag("CI")]
        [AllureSubSuite("UnitTest1-Automation")]
        public void Test_Entry_GET_ReturnsViewResultNullModel()
        {
            // Arrange
            var controller = new PartyController();

            // Act
            var result = controller.Entry();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewData.Model);
        }

        [AllureXunit(DisplayName = "Test_Entry_POST_InvalidModelState")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureTag("CI")]
        [AllureSubSuite("UnitTest1-Automation")]
        public void Test_Entry_POST_InvalidModelState()
        {
            // Arrange
            var controller = new PartyController();

            // Act
            var result = controller.Entry(null, null);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.IsType<SerializableError>(badRequestResult.Value);
        }

        [AllureXunit(DisplayName = "Test_Entry_POST_ValidModelState")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureTag("CI")]
        [AllureSubSuite("UnitTest1-Automation")]
        public void Test_Entry_POST_ValidModelState()
        {
            // Arrange
            string name = "Tom Cruise", membership = "Platinum";
            var controller = new PartyController();

            // Act
            var result = controller.Entry(name, membership);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<string>(viewResult.ViewData.Model);
        }
    }
}